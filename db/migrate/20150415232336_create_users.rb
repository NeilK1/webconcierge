class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :admin_login
      t.string :logout

      t.timestamps null: false
    end
  end
end
