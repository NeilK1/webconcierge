class PagesController < ApplicationController
  def about
  end

  def contact
  end

  def services
  end

  def shop
  end
end
