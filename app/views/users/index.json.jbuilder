json.array!(@users) do |user|
  json.extract! user, :id, :login, :admin_login, :logout
  json.url user_url(user, format: :json)
end
